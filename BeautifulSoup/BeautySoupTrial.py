# Program "THE STALKING APP YOU HAVE EVER DREAMED OF" vznikl jako vtip.

# Ve skutečnosti ale jde aplikaci, která instantně stáhne nejrelevantnější obrázky k zadanému heslu.
# To se může týkat, čehokoli. Například student potřebuje sadu expresionistických obrazů na další hodinu dějin umění,
# a tak do vstupu napíše: "expressionism art" a během několika vteřin má plnou složku obrazů a nemusí je tedy stahovat jednotlivě.
# Při potřebě obrázky zpětně ozdrojovat, si lze v odpovědním okně dohledat odkaz podle příslušného identifikátoru.

#
# Teď ale zpět k původnímu záměru projektu. Pokud chceme na svého kamaráda sehnat za krátkou chvíli kvanta kompromitujícího materiálu,
# stačí napsat do vstupu jeho jméno popřípadě i místo bydliště nebo název školy pro přiblížení se žádaným výsledkům.
# Program funguje velmi dobře při vyhledávání celebrit, univerzitních profesorů nebo lidí s unikátním jménem.
# Například heslo "damian hruban" nabízí bezednou studnici kompromitujících fotek.
# Naopak nedoporučujeme zadávat jméno vaší přítelkyně, protože možná byste se dověděli něco, co jste nikdy vědět nechtěli.




# Je potřeba stáhnout BeautifulSoup a nahrát další knihovny přes Project Interpreter v Settings

from bs4 import BeautifulSoup
import urllib.request as request
import json
import requests
import shutil

def input1():
    global a
    global no_spaces_a
    global search_name
    a = input()
    no_spaces_a = "%20".join(a.split(" "))    # přizpůsobí mezery webové adrese
    search_name = "_".join(a.split(" "))      # přizpůsobí mezery pro název stažených obrázků
    return no_spaces_a


def parse():

    try:

        #  je třeba nastavit existující cestu pro uložení stažených obrázků
        folder = r'C:\Users\Acer\PycharmProjects\cviko_1\BeautifulSoup\Downloaded_compromising_images' + '\\'
        #   tbm=isch ---> vyhledava obrazky,  %20 je mezera
        URL = 'https://www.google.com/search?q=' + no_spaces_a + '&tbm=isch'

        # zmena headers pro prochazeni vysledku vyhledavani googlu
        header = {
            'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"}
        response = request.urlopen(request.Request(URL, headers=header))
        soup = BeautifulSoup(response, 'html.parser')

        img_relevance = 1

        for a in soup.find_all('div', {'class':'rg_meta'}):
            stripped_a = a.get_text()       # ořízne html značky na zacatku a na konci <>
            a_dict = json.loads(stripped_a)  # převede json do slovníku


            r = requests.get(a_dict['ou'], stream=True, headers={'User-agent': 'Mozilla/5.0'})   # zmena headers pro parsovani
            if r.status_code == 200:
                full_name = folder + search_name + str(img_relevance) + ".png"
                with open(full_name, 'wb') as f:
                    r.raw.decode_content = True
                    shutil.copyfileobj(r.raw, f)
                print(search_name + str(img_relevance) + ".png:     " + a_dict['ou'])
            img_relevance += 1



    except Exception as e:
        print(str(e))

# smyčka pro nové vyhledávání
def loop_input():
    while True:
        input1()
        parse()
        print ("Images related to search \""+ search_name +"\" were successfully saved\n"+\
               "If you wish to download another google search, type it down:")


print("WELCOME TO THE STALKING APP YOU HAVE EVER DREAMED OF\n"+\
    "Type a desired query for Google (without diacritics)\n"+\
    "and we are going to save your time\n"+\
    "by parsing the most relevant images.\n"+\
    "(Change a path for saving the pictures to existing directory\n"+\
    "in function \"parse()\" by altering variable folder)\n"+\
    "Now, let the stalking begin!")

loop_input()







