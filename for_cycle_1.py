# Vytvorte pomoci for cyklu pulku pyramidy z hvezdicek. Vystup bude vypadat nasledovne
# *
# **
# ***
# ****
# *****


for i in range (0,6):
    print('*'*i)

    # pro kazdou hodnotu (i) z rozpeti (0,6) - druhe hranicni cislo neni zahrnuto, prvni ano
    # vytiskni pocet * vynasobeny zrovna projizdenym cislem