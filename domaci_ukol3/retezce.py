# 1 bod
# z retezce string1 vyberte pouze prvni tri slova a pridejte za ne tri tecky. Vypiste je
# spravny vypis: There are more...
string1 = 'There are more things in Heaven and Earth, Horatio, than are dreamt of in your philosophy.'

pocet_slov = 0
string1A = ""

for x in string1:
    string1A += x
    if x == " ":
        pocet_slov += 1
        if pocet_slov == 3:
            string1A=string1A[:-1]
            string1A += "..."
            break

print(string1A)

# 1 bod
# z retezce string1 vyberte pouze poslednich 11 znaku a vypiste je
# spravny vypis: philosophy.

string1B = string1[-11:]
print(string1B)



# 1 bod
# retezec string2 rozdelte podle carek a jednotlive casti vypiste
# spravny vypis:
# I wondered if that was how forgiveness budded; not with the fanfare of epiphany
# but with pain gathering its things
# packing up
# and slipping away unannounced in the middle of the night.
string2 = 'I wondered if that was how forgiveness budded; not with the fanfare of epiphany, but with pain gathering its things, packing up, and slipping away unannounced in the middle of the night.'

list2 = string2.split(",")

for x in list2:
    print(x)



# NEBO PRO ZACHOVANI ŘETĚZCE:

# string8 =   "I wondered if that was how forgiveness budded; not with the fanfare of epiphany, \n"+\
#             "but with pain gathering its things,\n"+\
#             "packing up,\n"+\
#             "and slipping away unannounced in the middle of the night."
# print(string8)
#
# NEBO:

# string8 = """
# I wondered if that was how forgiveness budded; not with the fanfare of epiphany,
# but with pain gathering its things,
# packing up,
# and slipping away unannounced in the middle of the night.
# """
# print(string8)


# 2 body
# v retezci string2 spocitejte cetnost jednotlivych pismen a pote vypiste jednotlive cetnosti i pismenem
# spravny vypis: a: 12
#                b: 2
# atd.

import string


abc_seznam = list(string.ascii_lowercase)  # importovany retezec malych pismen z abecedy
used_chars = {}                            # vsechny pouzite znaky ve string2 s cetnosti
used_letters = {}                          # jen pouzita pismena s cetnosti

for char in string2:
    if char.lower() not in used_chars:
        used_chars[char.lower()] = 1
    elif char.lower() in used_chars:
        used_chars[char.lower()] +=1

for k, v in used_chars.items():
    for letter in abc_seznam:
        if letter == k:
            used_letters[k]=v

used_letters_sorted = sorted(used_letters)    # seradi klice podle abecedy (ale do listu a ne do slovniku)

for x in used_letters_sorted:
    print (x,":", used_letters[x])



# 5 bodu
# retezec datetime1 predstavuje datum a cas ve formatu YYYYMMDDHHMMSS
# zparsujte dany retezec (rozdelte ho na rok, mesic, den, hodiny, minuty, sekundy a vytvorte z nej datum tak jak definuje
# funkce datetime. Pouzijete knihovnu a tridu datetime (https://docs.python.org/3/library/datetime.html#datetime.datetime)
# Potom slovy odpovezte na otazku: Co je to cas od epochy (UNIX epoch time)?
# Odpoved:
# Nasledne odectete zparsovany datum a cas od aktualniho casu (casu ziskaneho z pocitace pri zavolani jiste funkce)
# a vypocitejte kolik hodin cini rozdil a ten vypiste
datetime1 = '20181121191930'

import datetime

year =      int(datetime1[0:4])
month =     int(datetime1[4:6])
day =       int(datetime1[6:8])
hour =      int(datetime1[8:10])
minute =    int(datetime1[10:12])
second =    int(datetime1[-2:])



datum = datetime.datetime(year, month, day, hour, minute, second)
print(datum)
dnesni_datum = datetime.datetime.now()
print(dnesni_datum)
rozdil_dat = dnesni_datum - datum
print(rozdil_dat)






#    .datetime(year, month, day, hour=0, minute=0, second=0, microsecond=0, tzinfo=None, *, fold=0)





# 5 bodu
# v retezci string3 jsou slova prehazena. V promenne correct_string3 jsou slova spravne.
# vytvorte novou promennou, do ktere poskladate spravne retezec string3 podle retezce correct_string3
# nasledne vypocitej posun mist o ktere muselo byt slovo posunuto, aby bylo dano do spravne pozice.
# nereste pritom smer pohybu
# vypiste spravne poradi vety a jednotliva slova s jejich posunem
# napr. war: 8
#       the: 6
# atd.
string3 = 'war concerned itself with which Ministry The of Peace'
correct_string3 = 'The Ministry of Peace which concerned itself with war'


listA = string3.split(" ")
listC = correct_string3.split(" ")

# string4 = ''
# string4 = (listA[-3] +" "+ listA[-4] +" "+ listA[-2] +" "+ listA[-1] +" "+ listA[-5] +" "+ listA[1] +" "+ listA[2] +" "+ listA[3] +" "+ listA[0])

output_dict = {}
correct_list = []
for i in listC:
    for x in listA:
        if [x] == [i]:
            correct_list.append(x)
            output_dict[i] = abs(listA.index(x) - listC.index(i))



correct_string3B = " ".join(correct_list)
print(correct_string3B)


result = sorted(output_dict.items() , key=lambda t : t[1] , reverse=True)

for k,v in result:
    print(k,":", v)







