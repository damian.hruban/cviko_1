# vytvorte pole, kde budou nejake libovolne retezce
# pomoci funkce input() zjistete od uzivatele hodnotu, kterou chce v poli vyhledat
# vyhledejte pomoci cyklu a podminky hodnotu a pokud hodnotu najdete tak ukoncete prochazeni cyklu a hodnotu vypiste na obrazovku

"""
seznam = ['Ondra', 'Petr', 'Marek', 'Janek' ]
slovo = input ('Napiš slovo:')
for x in seznam:
    if slovo == x:
        print (slovo)


# napisu slovo -- pro hodnoty (x) ze seznamu --kdyz je slovo(napsane) hodnotou (x) ze seznamu --> vytiskni slovo



# HLEDANI STEJNYCH ZAPISU VE DVOU POLICH
items = ["aaa", 111, (4, 5), 2.01]
tests = [(4, 5), 3.14]

for key in tests:
    for item in items:
        if item == key:
            print(key, "was found")
            break
        else:
            print(key, "not found!")

# Vypise
#(4, 5) was found
#3.14 not found!
"""

spoluzaci = ["Jirka", "Vaclav", "Jara"]
fb_pratele = ["Petr", "Jirka", "Marek", "Vaclav"]
spoluzaci_na_fb = 0
neznami_na_fb = 0

for key in fb_pratele:
    for item in spoluzaci:
        if key == item:
            spoluzaci_na_fb += 1
            break
        else:
            neznami_na_fb += 1
print ("Pocet spoluzau na fb: ",spoluzaci_na_fb)
print ("Neprirazeno:          ",neznami_na_fb)
