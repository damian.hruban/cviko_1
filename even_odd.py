# vytvorte si pole alespon s 10 cisly typu integer
# vytvore cyklus, ktery bude prochazet polem a pomoci podminek bude zjistovat jestli je cislo sude nebo liche. Pokud bude sude, tak pricte 1 do promenne, ktera bude uchovavat pocet sudych hodnot a naopak pro licha cisla.
# Nakonec vypiste pocty lichych a sudych cisel v poli
"""
cisla = [1,2,3,4,5,6,7,8,9,10 ]


for x in cisla:
    if x%2 == 0:
        print ('sude')
    else:
        print ('liche')
"""

cisla = [1,2,3,4,5,6,7,8,9,10 ] #definuju si seznam(pole)
sude = 0                        #zadam pocatecni hodnotu sudych a lichych
liche = 0
for x in cisla:                 # pro hodnoty v seznamu
    if x%2 == 0:                    # kdyz je hodnota deleno 2 = 0
        sude +=1                        # k sudym prictu jedna
    else:                       # -"-
        liche += 1
print(sude,liche)               #vytisknu promenne sude a liche, ke kterym jsem pricital