fridge = [{'name': 'ham', 'price': 30, 'expired': False},
                                          {'name': 'salad', 'price': 45, 'expired': False},
                                          {'name': 'egg', 'price': 4, 'expired': False},
                                          {'name': 'wine', 'price': 200, 'expired': False},
                                          {'name': 'orange juice', 'price': 40, 'expired': False}]



def eat_food(name):
    """
    Funkce, ktera odstrani z lednice potravinu pokud v ni potravina byla
    :param (str) name: nazev potraviny, kterou budete odebirat
    :return (bool): vraci True, kdyz potravina byla odebrana z lednice. Jinak vraci False
    """
    for [x] in fridge:
        if name == fridge.name:
            remove (name)
            return True

print('Snedl jsem sunku:', eat_food('ham'))
print('Snedl jsem hovinko:', eat_food('poop'))