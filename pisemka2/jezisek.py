"""
Resenim teto pisemky bude zpracovat 3 dopisy (letter1, letter2 a letter3) od 3 deti, ktere pisou jeziskovi a nakoupit veci z dopisu.
V kazdem dopise je objekt, ktery je mozne koupit na eshopu (promenna stores).
Cilem je nakoupit jeziskovi v eshopech vsechny veci, o ktere si deti napsaly.
Jezisek ma omezeny rozpocet (promenna money), ale muze si vzit uver od banky (promenna bank).
Projdete dopisy deti a najdete v dopisech veci, ktere mate koupit. Veci jsou oddelene - (pomlckou) a jsou vzdy na konci vet.
V dopisech od deti jsou gramaticke chyby, takze bude potreba hledat varianty s i/y nebo ceske prepisy (pro plejstejsn budete hledat playstation).
Tyto veci se pote pokuste najit v eshopech a "koupit". Pri koupeni musite odecist jednu polozku z poctu dostupnych polozek na eshopu a odecist penize jeziska.
Preferujte nejlevnejsi moznost.
Pokud jezisek nebude mit na danou vec dost penez, bude si muset pujcit od banky (odectete penize z banky).
Nakonec programu vytvorite formatovany vypis do konzole, ktery bude obsahovat informace o vsech nakoupenych polozkach (obchod, cena).
Dale bude obsahovat kolik jeziskovy zbylo penez a kolik si pujcil od banky a o kolik vic zaplati na urocich.

SHRNUTI:
1. Zpracujete dopisy od deti (zohlednite gramaticke chyby a ceske tvary)
2. Vyhledate darky z dopisu na eshopu
3. Koupite nejlevnejsi darky na eshopech a odectete pocet polozek v obchode
4. Pokud jeziskovi nevyjdou penize tak si pujcite od banky
5. Vypisete informace o nakupech a jeziskovych financich

ZPUSOB IMPLEMENTACE JE NA VAS. SAMOTNE RESENI (FUNKCE) BUDETE MUSET VYMYSLET SAMI.
"""

bank = 1000000
rate = 19.9
money = 30000

letter1 = """Myly jezisku, na vanoce bych si pral -hodinky s vodotriskem"""
letter2 = """Na vanoce bich moc chtel -plejstejsn"""
letter3 = """Na vanoce bych si prala -ponika"""

stores = [
    {'name': 'Alza', 'products': [
        {'name': 'hodinky s vodotryskem', 'price': 150, 'no_items': 5},
        {'name': 'pocitac', 'price': 20000, 'no_items': 2},
        {'name': 'ponik', 'price': 50000, 'no_items': 1}
    ]},
    {'name': 'CZC', 'products': [
        {'name': 'playstation', 'price': 9000, 'no_items': 5},
        {'name': 'Dobroty Ladi Hrusky', 'price': 10, 'no_items': 100},
        {'name': 'ponik', 'price': 70000, 'no_items': 1}
    ]}
]




l1 = letter1.split(" ")
l2 = letter2.split(" ")
l3 = letter3.split(" ")


def oprava(l):
    if l[-1] == "-plejstejsn":
        l[-1] = "playstation"
        return l
    if l[-1] == "vodotriskem":
        l[-1] = "vodotryskem"
        return l
    if l[-1] == "-ponika":
        l[-1] = "ponik"
        return l

print(oprava(l1))
print(oprava(l2))
print(oprava(l3))

vec1 = l1[-3] +" "+ l1[-2] +" "+ l1[-1]
vec2 = l2[-1]
vec3 = l3[-1]

vec1 = vec1[1:]
vec2 = vec2[0:]
vec3 = vec3[0:]

print(vec1) #hodinky s vodotryskem
print(vec2) #plazstation
print(vec3) #poník



def find(vec):
    for x in stores:
        for y in x["products"]:
            if y["name"] == vec:
                buy(y["price"])
                pujcit()
                y["no_items"] -=1
                return y

def buy (a):
    global money
    money  -=  a
    return money


def pujcit():
    global money
    global bank
    if money <= 0:
        bank += money
        money -= money
        return bank

dluh = 0

def owe(prachy):

    global dluh
    dluh_bez_uroku = 1000000 - prachy
    dluh = 0.199*dluh_bez_uroku +dluh_bez_uroku
    return dluh

print(find(vec1))
print(find(vec2))
print(find(vec3))
print(" ")

print("Ježíškova peněženka:", money)
print("Zůstatek v bance po nákupu:", bank)
print("Ježíškův dluh i s úrokem:", owe(bank))




























