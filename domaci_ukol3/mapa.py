# 20 bodu
'''Kdo hral textovou hru od Googlu tak bude v obraze. Cilem bude udelat mapu. Muzete si udelat vlastni nebo pouzit moji.
Mapa se bude skládat z několika částí a bude reprezentována textem. Obrázek mapy se nachází pod tímto textem
Cilem toho cviceni bude spise vymyslet jak toto cviceni vyresit. Samotna implementace nebude tak narocna.

Prace s dvourozmernym polem je seznamem (polem) je jednoducha

pole = [['a','b','c','d'],
        ['e','f','g','h'],
        ['i','j','k','l']]

Dvourozmerne pole obsahuje radky a sloupce. Kazda hodnota ma svuj jedinecny index definovany indexem radku a sloupce
napr. pole[0][1] vybere hodnotu na prvni radku ve druhem sloupci 'b'.
      pole[1][3] vybere hodnotu na druhem radku ve ctvrtem sloupci 'h'.
Vice informaci najdete na internetu: https://www.tutorialspoint.com/python/python_2darray.htm

Mapa (dvourozmerne pole) predstavuje bludiste. Vase postava se na zacatku hry bude nachazet na pozici pismene S (index [7][0]).
Cílem je dostat se do cile, ktery oznacuje pismeno F (index [0][9]).
Budete se ptat uzivatele stejne jako v textove hre na informaci o tom kterym smerem chce jit - nahoru, doprava, dolu, doleva.
Po každém zadání posunu vykreslíte mapu, kde bude zobrazena pozice postavicky v mape a pozici v jejim primem okoli. Bude to vypadat asi takto:

      ?????
      ?***?
      ?*P.?
      ?*.*?
      ?????

Tečky představují cestu (místo kam může postavička stoupnout). Hvězdičky představují zeď, tam postavička nemůže vstoupit.
Ostatní okolí postavy (zbytek mapy) bude ukryt postavě a budou ho představovat otazníky.

# BONUS: 5 bodů
# Udělejte verzi, kde se bude ukládat a při vykreslování mapy zobrazovat i místo, které postava už prošla a tudíž ho zná.
'''

game_map = [['*','*','*','*','*','*','.','.','.','F'],
            ['*','*','*','*','.','.','.','*','*','*'],
            ['.','.','.','.','.','*','*','*','*','*'],
            ['.','*','*','*','*','*','*','*','.','.'],
            ['.','.','.','*','*','.','.','.','.','.'],
            ['*','*','.','.','.','.','*','*','*','*'],
            ['*','*','.','*','*','*','*','*','*','*'],
            ['S','.','.','.','.','.','.','.','.','.']]

def map_overview():
    for l in game_map:
        for y in l:
            print(y, end=" ")
        print()



line = 7
column = 0




def move():
    while 1:
        global line
        global column

        if a == "w":
            line -= 1
            if line <= 0:
                line += 1
                return line
            return line


        if a== "s":
            line += 1
            if line >= 7:       # osetreni, aby neslo postoupit pres max index
                line -= 1
                return line
            return line

        if a == "d":
            column += 1
            if column >= 9:      # osetreni, aby neslo postoupit pres max index
                column -= 1
                return column
            return column
        if a == "a":
            column -= 1
            if column <= 0:
                column += 0
                return column
            return column
        else:
            return print("Choose the direction you'd like to go.\n"+\
                  "for going UP hit:    'w'\n"+\
                  "for going DOWN hit:  's'\n"+\
                  "for going RIGHT hit: 'd'\n"+\
                  "for going LEFT hit:  'a'\n"+\
                    "________________________")




def move_back():
    while 1:
        global line
        global column
        if a == "w":
            line += 1
            return line
        if a== "s":
            line -= 1
            return line
        if a == "d":
            column -= 1
            return column
        if a == "a":
            column += 1
            return column






# definice pole, ktere hrac po kazdem tahu uvidi
# 123
# 4 5
# 678


# indexy ukazuji na druhou stranu (pres hranu) -OMEZIT-->zasadit do kodu a melo by fungovat


position = ""
pos1 = ""
pos2 = ""
pos3 = ""
pos4 = ""
pos5 = ""
pos6 = ""
pos7 = ""
pos8 = ""

def view():
    global position
    global pos1
    global pos2
    global pos3
    global pos4
    global pos5
    global pos6
    global pos7
    global pos8

    try:
        position = game_map[line][column]


    except IndexError:
        position = " "
    try:
        pos1 = game_map[line - 1][column - 1]
    except IndexError:
        pos1 = " "
    try:
        pos2 = game_map[line - 1][column]
    except IndexError:
        pos2 = " "
    try:
        pos3 = game_map[line - 1][column + 1]
    except IndexError:
        pos3 = " "
    try:
        pos4 = game_map[line][column - 1]
    except IndexError:
        pos4 = " "
    try:
        pos5 = game_map[line][column + 1]
    except IndexError:
        pos5 = " "
    try:
        pos6 = game_map[line + 1][column - 1]
    except IndexError:
        pos6 = " "
    try:
        pos7 = game_map[line + 1][column]
    except IndexError:
        pos7 = " "
    try:
        pos8 = game_map[line + 1][column + 1]
    except IndexError:
        pos8 = " "




def draw():
    print("? ? ? ? ?")
    print("?", pos1, pos2, pos3, "?")
    print("?", pos4,"p", pos5, "?")     # pri komplikaci vratit "p" na position
    print("?", pos6, pos7, pos8, "?")
    print("? ? ? ? ?")

def save_way():                     # ukládá prošlou trasu
    game_map[line][column] = "O"
    return game_map




def walls_restriction():

    if game_map[line][column] == "*":
        return False
    else:
        return True



def move_loop_till_F():
    while 1:
        global a
        if position != "F":
            a = input()
            move()                              # pohyb

            if walls_restriction() is True:     # když je pravda, že při pohybu nenarazil na zeď...
                view()                          # definuje rozlhed podle indexů od pozice, kde hráč stojí
                draw()                          # vykreslí rozhled
                save_way()                      # ukládá prošlou trasu

            elif walls_restriction() is False:  # když narazil na zeď...
                move_back()                     # vrátí se zpátky, potom, co narazil na zeď
                view()
                draw()
        else:
            print("Congrats, you have finally escaped from the labirinth!")
            break



move_loop_till_F()

