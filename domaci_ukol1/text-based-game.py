"""
Vaším úkolem je vytvořit textovou hru:
https://cs.wikipedia.org/wiki/Textov%C3%A1_hra

Při tvorbě použijte proměnné, funkci input(), funkci print(), alespoň 4 podmínky (jedna z podmínek bude
obsahovat i elif větev a zanořenou podmínku včetně operatorů and a or).

Můžete udělat hru na jakékoliv téma. Hlavně ať to není nudné a splní to minimální podmínky zadání!

Celkem to bude za 5 bodů. Dalších 5 bonusových bodů (nezapočítávají se do základu)
můžete získat za správné použití 5 libovolných funkcí pro řětězce, čísla (např. split() apod.). Další funkce zkuste vygooglit na internetu.
Použité funkce se musí vztahovat k logice hry.




if, elif, else -- OK
vnořené podmínky -- OK
AND, OR -- OK

Pouzite funkce:

capitalize()
strip()
len()
upper()
bold

"""


# UVODNI PLOCHA A SPUSTENI

print()
print('   KLASICKÝ STŘEDEČNÍ RÁNO   ')
print('_____________________________')
print( )
print(' Pro spuštění napiš:\'hrat\'')


while 1:
    hrat = input()
    if hrat =='hrat': break
    else: print ('Vedle, zkus to znova.')


# UDAJE O HRACI
print ('Než začneme, musíš vyplnit několik osobních údajů. Nejprve se tě zeptám na pohlaví.')
while 1:
    pohlavi = input ()
    if pohlavi in ('muz', 'zena'): break

# definice tucneho textu
bold = "\033[1m"
reset = "\033[0;0m"


# OSOBNI UDAJE

print ('Jak se jmenuješ?')
jmeno = input()
if len(jmeno) > 7:
     print ('Teda! ' + bold + jmeno.capitalize() + reset + '? To je dlouhý jméno. ' + str (len(jmeno))+ ' písmen. Tak bych se nechtěl podepisovat, ale zní dobře.')
else:
    if pohlavi == 'zena':
        print('Opravdu,' + bold + jmeno.upper() + reset + '? ' + str (len(jmeno))+ ' tak libozvučných písmen! Stejně se jmenuje i moje babička. Vskutku krásné jméno!')
    if pohlavi == 'muz':
        print('Opravdu, ' + bold + jmeno.upper() + reset + '? ' + str (len(jmeno))+ ' tak libozvučných písmen! Stejně se jmenuje i můj děda. Vskutku krásné jméno!')


# START HRY
print ('Ale pojďme už na to! Napiš: \'start\'')
while 1:
    start = input()
    if start == 'start': break

print('______________________________')
print('Středa... Je už skoro poledne.')
print('Probudil tě ostrý paprsek pronikající střešním oknem. Jasné světlo rozzářilo bílé kachle na stěnách koupelny. ')
print('Vůbec netušíš, kde se nacházíš a nesnesitelně ti třeští hlava. Něco tě tlačí v zádech...')
print('Co uděláš?')
print('__________________________________________')
print('Otočím se na bok a budu spát dál: \"spat\"')
print ('Porozhlédnu se po místnosti: \"rozhlednout\"')
while 2:
    cinnost = input()
    if cinnost =='rozhlednout':
        break

    if cinnost == 'spat':
        print ('Ztratil jsi několik dalších hodin, gratuluju. V místnosti už je šero, ale vůbec sis nepomohl, hlava tě bolí pořád stejně.')
        print('__________________________________________')
        print('Otočím se na druhý bok a budu spát dál: \"spat\"')
        print('Porozhlédnu se po místnosti: \"rozhlednout\"')
        while 2:
            cinnost2 = input()
            if cinnost2 == 'spat':
                print ('Už jsi naspal tolik hodin, že nemůžeš usnout.')
                print ('_____________________________')
                print ('Porozhlédnu se po místnosti: \"rozhlednout\"')
                while 2:
                    cinnost3 = input()
                    if cinnost3 != 'rozhlednout':
                        break
                    break
                break
            break


        if cinnost2 == 'rozhlednout':
            break


print('To tvrdé, z čeho tě bolí záda, je keramická vana, ve které jsi spal.')
print('Po místnosti se válí pár prázdných lahví a několik kusů špinavého oblečení. Svoje boty ale nikde nevidíš.')
print('Tvůj  mobil leží z nějakého důvodu na zemi v koutě.')
print('Co uděláš?')
print('__________')
print('Zavolám mámě o pomoc \"volat\"')
print('Prohlédnu si fotky \"fotky\"')

while 2:
    cinnost4 = input ()
    if cinnost4 == 'fotky':
        break
    if cinnost4 == 'volat':
        print('Mamka tě nenechá domluvit o tvém neštěstí a už tě zve na nedělní svíčkovou před tím, než to znenadání v půlce věty položí.')
        print('Moc těm internetům totiž ještě nerozumí.')

        while 1:
            cinnost5 = input()
            if cinnost5 == 'fotky':
                break
            else:
                print('Tak to nepůjde.')

print('Vzpomínky se vrací a okna vyplňují. Velice se stydíš, když si prohlížíš fotky ze včerejšího večírku překypujícího zhýralostmi.')
print('Obzvlášť tě překvapila selfie z ohromného sálu plného LCD obrazovek a zaneprázdněných lidí v šatech a kravatách.')
print('Tvůj kamarád v pozadí mává nějakými lístky...')
print('_____________________________________________')
print('Prozkoumám fotku detailněji\"zoom\"')
print('Prohlédnu si další fotky\"dalsi\"')
print('Podívám se ven z pokoje \"ven\"')

while 1:
    cinnost6 = input()
    if cinnost6 == 'dalsi' or cinnost6 not in ('zoom', 'ven'):
        print('Nic zajímavějšího už jsi včera nenafotil')
    elif cinnost6 == 'ven':
        print('Vezmeš za kliku, ale dveře jsou zamčeny. Trochu znervózníš a po pár minutách klepání na dveře a volání')
        print('si opět sedneš do vany s mobilem v ruce a snažíš se z fotek složit střípky vzpomínek.')
    else:
        break


str =('         Ohromný sál z fotky byl terminál. Na jednom z lístků, se kterými mával tvůj kamarád      ')
print(str.strip())
print('je napsané jméno ' + jmeno.upper() + ' *****. Jsou to letenky a jsou do Barcelony...')
print('')
print('V tu ránu vtrhne do koupelny rozezlená uklízečka a začne tě ostřelovat velmi svižnou španělštinou.')
print('')
print('_____________________________________________________')
print('_____________________KONEC HRY_______________________')
print('Tady hra končí. Venku už je tma, ty jsi v Katalánsku ')
print('          a večerní cviko jsi zase zmeškal.          ')
